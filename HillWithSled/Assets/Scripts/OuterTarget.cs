﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// These all inherit from target so they can easily call it's score function, they just check if they
// collide with the player and send the score up if they do.

// One thing worth mentioning is that the sled parts have the "scores" tag while the rider parts have the 
// "Player" tag. This is so these classes can not score the sled if the player ejects, and only keep track
// of the rider themself. (It can be irritating if you jump off the sled but the sled hits first and you
// get a low score.)
public class OuterTarget : Target
{
    private int points;
    // Start is called before the first frame update
    void Start()
    {
        points = 10;
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if ((collision.gameObject.tag == "scores" && !Eject.ejected) || collision.gameObject.tag == "Player")
            HitTarget(points);
    }
}