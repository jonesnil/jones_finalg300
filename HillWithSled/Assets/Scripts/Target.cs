﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    //Easily accessible place to keep the score, which some other classes need access to.
    public static int score;

    //The other target classes use this to check if their score is the highest.
    protected static void HitTarget(int points)
    {
        //Ensures that the score is the highest part of the target you hit.
        if (points > score)
            score = points;
    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
    }
}
