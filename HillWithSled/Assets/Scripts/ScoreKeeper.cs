﻿// Cool font comes from here:
// https://www.1001freefonts.com/action-man.font
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ScoreKeeper : MonoBehaviour
{
    Image background;
    Text scoreDisplay;
    Text comment;
    Text helper;

    // This keeps track of whether the player has hit the target, which some other
    // classes need to know.
    public static bool levelOver;

    private Scene currentScene;
    private string sceneName;
    private string nextSceneName;
    private int currentLevel;

    // Start is called before the first frame update
    void Start()
    {
        levelOver = false;
        background = this.transform.GetChild(0).GetComponent<Image>();
        scoreDisplay = this.transform.GetChild(1).GetComponent<Text>();
        comment = this.transform.GetChild(2).GetComponent<Text>();
        helper = this.transform.GetChild(3).GetComponent<Text>();

        // This scene stuff is taken from my Penny Pixel game, so I brought the 
        // comments with it.

        //This just grabs the scene we're in and gets its name.
        this.currentScene = SceneManager.GetActiveScene();
        this.sceneName = currentScene.name;

        // All the levels (scenes) are called Level"X" with "X" being the level number. This just
        // grabs that "X" so I know what level the player is on.
        currentLevel = Convert.ToInt32(sceneName.Substring(sceneName.Length - 1));

        // This takes that level, adds 1 and puts it into the Level"X" format so it
        // knows what the next scene is called.
        nextSceneName = String.Format("Level{0}", currentLevel + 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Target.score != 0)
        {

            // Keeps track of your totalScore throughout the game, it's in the if statement
            // so it only happens once per target hit. 
            if (!levelOver)
            {
                ImmortalScoreKeeper.totalScore += Target.score;
            }

            levelOver = true;

            // Stop time when they hit the target
            Time.timeScale = 0f;

            // Lerp the background to red for the UI
            Color opaqueBackground = background.color;
            opaqueBackground.a = Mathf.Lerp(opaqueBackground.a, 1, .045f);
            background.color = opaqueBackground;

            // Get the score from the Target class and display it (Unless that was the 
            // last level, in which case display total score across game)
            int score = Target.score;

            if (currentLevel + 1 <= SceneManager.sceneCountInBuildSettings)
            {
                scoreDisplay.text = score.ToString();
                // Give a comment based on the score
                if (score == 10)
                    comment.text = "AIM!! YOU GOTTA AIM!";

                else if (score == 50)
                    comment.text = "NOT BAD, DUDE!";

                else if (score == 100)
                    comment.text = "RADICAL!!!";
            }

            // Just a small acknowledgement that you finished the game and your total score.

            else
            {
                scoreDisplay.text = ImmortalScoreKeeper.totalScore.ToString();

                // Give a comment based on the total score
                if (ImmortalScoreKeeper.totalScore < 200)
                    comment.text = "WHO LET MY GRANDMA ON THIS??";

                else if (ImmortalScoreKeeper.totalScore >=  200)
                    comment.text = "THAT'S A SOLID C+, DUDE!";

                else if (ImmortalScoreKeeper.totalScore >= 400)
                    comment.text = "YOU'RE ON YOUR WAY TO THE PRO LEAGUE, KID!";

                else if (ImmortalScoreKeeper.totalScore >= 600)
                    comment.text = "YOU- ARE YOU SERIOUS?? YOU'RE A LEGEND DUDE!!";
            }
            

            // Help the player with a message about their spot in the game (after checking if there
            // is another level to go to or not)
            if (currentLevel + 1 <= SceneManager.sceneCountInBuildSettings)
                helper.text = "PRESS R TO RESTART OR TAB TO TRY THE NEXT LEVEL!";
            else
                helper.text = "THAT'S A WRAP, GOOD GAME BOSS! PRESS R TO SEE THAT WICKED RAMP AGAIN! (OR PRESS ESCAPE IF YOU'RE LAME)";
           
            // If there is a next level and the player presses tab, go to the next level.
            if (Input.GetKey(KeyCode.Tab))
            {
                // Have to do this in case the player lands the first target, because if 
                // this isn't false the game will be frozen to accomodate the UI at the beginning
                // of the next level. (It's hard to explain, but trust me it's important.)
                ImmortalScoreKeeper.firstLaunch = false;

                //If this is true the next level exists
                if (currentLevel + 1 <= SceneManager.sceneCountInBuildSettings)
                    //If the next level exists, load it.
                    SceneManager.LoadScene(nextSceneName, LoadSceneMode.Single);
            }
        }
    }
}
