﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmortalScoreKeeper : MonoBehaviour
{
    static public ImmortalScoreKeeper instanceOfThis;
    static public int totalScore;
    static public bool firstLaunch;
    // Start is called before the first frame update
    void Start()
    {
        totalScore = 0;
        firstLaunch = true;

        // This is also from my Penny Pixel game, it was simple to bring over so I thought I
        // might as well.

        // Tells Unity to not get rid of this when I load a new scene.
        DontDestroyOnLoad(this);
        //This block just says "If I exist already, destroy me. If I don't, let me be."
        if (ImmortalScoreKeeper.instanceOfThis == null)
        {
            ImmortalScoreKeeper.instanceOfThis = this;
        }
        else
        {
            firstLaunch = false;
            Object.Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
