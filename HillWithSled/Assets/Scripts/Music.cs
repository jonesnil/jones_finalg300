﻿//
//
// Awesome song comes from here: https://www.fesliyanstudios.com/royalty-free-music/download/crazy-drummer/218
//
//
/////////////////////////////////////////////////////////////////////////


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    private static Music soundInstance;
    private void Awake()
    {
        // This is also from my Penny Pixel game, it was simple to bring over so I thought I
        // might as well.

        // Tells Unity to not get rid of this when I load a new scene.
        DontDestroyOnLoad(this);

        // So, the first scene contains this sound game object which can't be destroyed by
        // loading a scene. But, if you die in the first scene, the first scene is reloaded.
        // This means I have to do something to stop a bunch of these from amassing if you die
        // in the first scene, because this won't be destroyed and a new version of it will be 
        // loaded.

        //This block just says "If I exist already, destroy me. If I don't, let me be."
        if (soundInstance == null)
        {
            soundInstance = this;
        }
        else
        {
            Object.Destroy(this.gameObject);
        }
    }
}
