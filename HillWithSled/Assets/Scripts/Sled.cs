﻿//
//
// Cool skidding sound comes from here: http://soundbible.com/719-Skidding.html
// Equally cool rocket noise from here: https://freesound.org/people/qubodup/sounds/146770/
//
///////////////////////////////////////////////////////////////////////////////////


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Sled : MonoBehaviour
{
    static public int score;
    private Rigidbody rb;
    private ParticleSystem rightBooster;
    private ParticleSystem leftBooster;
    private AudioSource skid;
    private AudioSource rocket;
    private AudioSource song;
    private GameObject head;
    private GameObject startTips;
    private Text missedHelper;

    //Important for keeping track of if the music/game should be paused for starting UI.
    private bool newGame;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;

        score = 0;
        rb = this.GetComponent<Rigidbody>();
        leftBooster = this.transform.GetChild(2).transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
        rightBooster = this.transform.GetChild(3).transform.GetChild(0).gameObject.GetComponent<ParticleSystem>();
        skid = this.transform.GetChild(5).GetComponent<AudioSource>();
        rocket = this.transform.GetChild(6).GetComponent<AudioSource>();
        song = GameObject.Find("Music").GetComponent<AudioSource>();
        head = GameObject.Find("Head");
        startTips = GameObject.Find("StartTips");
        missedHelper = GameObject.Find("Restart").GetComponent<Text>();
        newGame = true;

        // At least for me, the game was bursting my eardrums at 100 volume before, so I thought this
        // would spare some headphone users that trauma.
        AudioListener.volume = .1f;
    }

    // Update is called once per frame
    void Update()
    {
        // This gets rid of the tutorial canvas when it's the first launch and the player hits tab. It also starts time
        // and the music again.
        if ((ImmortalScoreKeeper.firstLaunch && Input.GetKey(KeyCode.Tab)))
        {
            startTips.SetActive(false);
            Time.timeScale = 1f;
            song.Play();
            newGame = false;
        }

        // This just gets rid of the UI, when time and the music haven't been touched (because it's a new load of the scene.)
        if (!ImmortalScoreKeeper.firstLaunch)
        {
            // After the first level startTips won't exist, this stops the null reference exception from that
            if(startTips != null)
                startTips.SetActive(false);
        }

        // This stops the sound effects and game from running while the player reads the tutorial text.
        if (ImmortalScoreKeeper.firstLaunch && newGame)
        {
            Time.timeScale = 0f;
            song.Pause();
        }

        // Let player quit on Escape
        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();

        // Let player reload game with R
        Scene scene = SceneManager.GetActiveScene();
        if (Input.GetKey(KeyCode.R))
        {
            // Subtracts your current score from total score if you already hit the target,
            // so your total score is just what was earned in your last play of each level.
            if (ScoreKeeper.levelOver)
                ImmortalScoreKeeper.totalScore -= Target.score;

            SceneManager.LoadScene(scene.name);
            Time.timeScale = 1f;
        }

        // Keep track of whether you're on the ground so I can do a cool skid sound effect,
        // and also so I can only allow you to scoot the sled when you're grounded (though
        // you can always use rockets/eject.)
        bool onGround = Physics.Raycast(this.transform.position, -this.transform.up, 2);
        bool onGroundUpsideDown = Physics.Raycast(this.transform.position, this.transform.up, 2);

        Vector3 movement = new Vector3(0, 0, 0);

        if (onGround || onGroundUpsideDown)
        {
            // Get forward movement
            if (Input.GetButton("Vertical"))
            {
                movement = this.transform.forward.normalized * -12 * Input.GetAxis("Vertical");
                rb.AddForce(movement);
            }

            // Get Left/Right Movement
            if (Input.GetButton("Horizontal"))
            {
                if (onGroundUpsideDown)
                    movement = this.transform.right.normalized * 24 * Input.GetAxis("Horizontal");
                else
                    movement = this.transform.right.normalized * -24 * Input.GetAxis("Horizontal");
                rb.AddForce(movement);
            }

            // Play skid sound effect, and make the volume proportionate to the player's speed
            // on the ground (faster movement = more friction = more skid)
            if (!skid.isPlaying) skid.Play();
            else skid.volume = rb.velocity.magnitude/50;

        }

        // If you aren't on the ground, stop the skid sound
        else
            skid.Stop();

        // If the level is done, stop the skid sound (it's annoying on the outro screen)
        if (ScoreKeeper.levelOver)
            skid.Stop();

        // Activates the rocket boosters and starts their particle effect/sound effect
        if (Input.GetKeyDown(KeyCode.Space))
        {
            leftBooster.Play();
            rightBooster.Play();
            if (!rocket.isPlaying)
                rocket.Play();
        }

        if (Input.GetKey(KeyCode.Space))
        {
            movement = this.transform.forward.normalized * -60;
            rb.AddForce(movement);
        }

        // If space isn't being held anymore, stop the particle effect and sound effect 
        // for the rocket.
        if (Input.GetKeyUp(KeyCode.Space))
        {
            leftBooster.Stop();
            rightBooster.Stop();
            rocket.Stop();
        }
        
        // Tell the player to restart if they are in the sled and are on the ground
        if (transform.position.y < -49 && !Eject.ejected)
            missedHelper.text = "PRESS R TO RESTART, DUDE!!";

        // Tell the player to restart if they jumped off the sled and they are on the ground
        else if(head.transform.position.y < -49 && Eject.ejected)
            missedHelper.text = "PRESS R TO RESTART, DUDE!!";

        // Gets rid of the restart text if you hit the target (sometimes you hit the ground before hitting
        // the target.)
        if (ScoreKeeper.levelOver)
            missedHelper.text = "";
    }

}
