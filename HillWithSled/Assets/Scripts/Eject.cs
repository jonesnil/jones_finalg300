﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eject : MonoBehaviour
{
    private CharacterJoint sledConnection;
    private Rigidbody pelvis;
    private GameObject sled;

    // This keeps track of if the player jumped off the sled, a few other classes need
    // to know that.
    static public bool ejected;

    // Start is called before the first frame update
    void Start()
    {
        ejected = false;
        sledConnection = this.GetComponent<CharacterJoint>();
        pelvis = this.GetComponent<Rigidbody>();
        sled = GameObject.Find("Sled");
    }

    // Update is called once per frame
    void Update()
    {

        // If the player hasn't already ejected and they hit E, break the joint connecting them to the sled
        // and launch them.
        if (!ejected && Input.GetKey(KeyCode.E))
        {
            Destroy(sledConnection);
            ejected = true;

            Vector3 ejection = sled.transform.up * 100f;
            pelvis.AddForce(ejection);
        }

    }
}
